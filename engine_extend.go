package xorm

import (
	"context"
	"database/sql"
	"xorm.io/xorm/core"
	"xorm.io/xorm/dialects"
	"xorm.io/xorm/schemas"
)

/*
  Engine的扩展类：4color
*/

// 获取自定义的上下文
func (engine *Engine) GetDefaultContext() context.Context {
	return engine.defaultContext
}

//scanStringInterface

func (engine *Engine) ScanStringInterfaceExt(rows *core.Rows, fields []string, types []*sql.ColumnType) ([]interface{}, error) {
	return engine.scanStringInterface(rows, fields, types)
}

func (engine *Engine) FormatBoolExt(s string, dstDialect dialects.Dialect) string {
	if dstDialect.URI().DBType == schemas.MSSQL {
		switch s {
		case "true":
			return "1"
		case "false":
			return "0"
		}
	}
	return s
}
