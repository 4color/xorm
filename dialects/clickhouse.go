// Copyright 2020 The Xorm Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package dialects

import (
	"context"
	"database/sql"
	"errors"
	"net/url"
	"regexp"
	"strings"

	"xorm.io/xorm/core"
	"xorm.io/xorm/schemas"
)

type clickhouse struct {
	Base
}

func (db *clickhouse) Init(uri *URI) error {
	return db.Base.Init(db, uri)
}

func (db *clickhouse) Version(ctx context.Context, queryer core.Queryer) (*schemas.Version, error) {
	rows, err := queryer.QueryContext(ctx, "SELECT version()")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var version string
	if !rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}
		return nil, errors.New("unknow version")
	}

	if err := rows.Scan(&version); err != nil {
		return nil, err
	}

	return &schemas.Version{
		Number:  strings.TrimPrefix(version, "v"),
		Edition: "Clickhouse",
	}, nil

	return nil, errors.New("unknow database version")
}

func (db *clickhouse) ColumnTypeKind(t string) int {
	switch strings.ToUpper(t) {
	case "DATETIME", "TIMESTAMP":
		return schemas.TIME_TYPE
	case "VARCHAR", "TEXT":
		return schemas.TEXT_TYPE
	case "BIGINT", "BIGSERIAL", "SMALLINT", "INT", "INT8", "INT4", "INTEGER", "SERIAL", "FLOAT", "FLOAT4", "REAL", "DOUBLE PRECISION":
		return schemas.NUMERIC_TYPE
	case "BOOL":
		return schemas.BOOL_TYPE
	default:
		return schemas.UNKNOW_TYPE
	}
}

func (db *clickhouse) IsReserved(name string) bool {
	return false
}

func (db *clickhouse) SQLType(c *schemas.Column) string {
	return ""
}

func (db *clickhouse) SetQuotePolicy(quotePolicy QuotePolicy) {
}

func (*clickhouse) AutoIncrStr() string {
	return ""
}

func (*clickhouse) CreateTableSQL(t *schemas.Table, tableName string) ([]string, bool) {
	return nil, false
}

func (*clickhouse) IsTableExist(queryer core.Queryer, ctx context.Context, tableName string) (bool, error) {
	return false, nil
}

func (*clickhouse) Filters() []Filter {
	return []Filter{}
}

func (*clickhouse) GetColumns(core.Queryer, context.Context, string) ([]string, map[string]*schemas.Column, error) {
	return nil, nil, nil
}

func (db *clickhouse) GetIndexes(queryer core.Queryer, ctx context.Context, tableName string) (map[string]*schemas.Index, error) {
	return nil, nil
}

func (db *clickhouse) IndexCheckSQL(tableName, idxName string) (string, []interface{}) {
	return "", nil
}

func (db *clickhouse) GetTables(queryer core.Queryer, ctx context.Context) ([]*schemas.Table, error) {
	return nil, nil
}

// ParseClickHouse parsed clickhouse connection string
// tcp://host1:9000?username=user&password=qwerty&database=clicks&read_timeout=10&write_timeout=20&alt_hosts=host2:9000,host3:9000
func ParseClickHouse(connStr string) (*URI, error) {
	u, err := url.Parse(connStr)
	if err != nil {
		return nil, err
	}
	forms := u.Query()
	return &URI{
		DBType: schemas.CLICKHOUSE,
		Proto:  u.Scheme,
		Host:   u.Hostname(),
		Port:   u.Port(),
		DBName: forms.Get("database"),
		User:   forms.Get("username"),
		Passwd: forms.Get("password"),
	}, nil
}

type clickhouseDriver struct {
	baseDriver
}

func (g *clickhouseDriver) Features() *DriverFeatures {
	return &DriverFeatures{
		SupportReturnInsertedID: false,
	}
}

func (g *clickhouseDriver) Parse(driverName, dataSourceName string) (*URI, error) {
	db := &URI{DBType: schemas.CLICKHOUSE}
	dsnPattern := regexp.MustCompile(
		`^(?:(?P<user>.*?)(?::(?P<passwd>.*))?@)?` + // [user[:password]@]
			`(?:(?P<net>[^\(]*)(?:\((?P<addr>[^\)]*)\))?)?` + // [net[(addr)]]
			`\/(?P<dbname>.*?)` + // /dbname
			`(?:\?(?P<params>[^\?]*))?$`) // [?param1=value1&paramN=valueN]
	matches := dsnPattern.FindStringSubmatch(dataSourceName)
	// tlsConfigRegister := make(map[string]*tls.Config)
	names := dsnPattern.SubexpNames()

	for i, match := range matches {
		if names[i] == "dbname" {
			db.DBName = match
		}
	}
	if db.DBName == "" {
		return nil, errors.New("dbname is empty")
	}
	return db, nil
}

func (g *clickhouseDriver) GenScanResult(colType string) (interface{}, error) {
	switch colType {
	case "CHAR", "NCHAR", "VARCHAR", "VARCHAR2", "NVARCHAR2", "LONG", "CLOB", "NCLOB":
		var s sql.NullString
		return &s, nil
	case "NUMBER":
		var s sql.NullString
		return &s, nil
	case "DATE":
		var s sql.NullTime
		return &s, nil
	case "BLOB":
		var r sql.RawBytes
		return &r, nil
	default:
		var r sql.RawBytes
		return &r, nil
	}
}
