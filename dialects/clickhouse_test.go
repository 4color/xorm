// Copyright 2020 The Xorm Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package dialects

import (
	"reflect"
	"testing"
)

func TestParseClickHouse(t *testing.T) {
	tests := []struct {
		in       string
		expected string
		valid    bool
	}{
		{"http://default:clickhouse@gisquest.com@192.168.9.82:8123/zrzysc", "zrzysc", true},
	}

	driver := QueryDriver("clickhouse")
	for _, test := range tests {
		t.Run(test.in, func(t *testing.T) {
			uri, err := driver.Parse("clickhouse", test.in)

			if err != nil && test.valid {
				t.Errorf("%q got unexpected error: %s", test.in, err)
			} else if err == nil && !reflect.DeepEqual(test.expected, uri.DBName) {
				t.Errorf("%q got: %#v want: %#v", test.in, uri.DBName, test.expected)
			}
		})
	}

}
